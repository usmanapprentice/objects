//check if object has property............
function hasProperty(obj, property) {
    var prop;
    for (prop in obj) {
        if (prop === property) {
            return true;
        }
    }
    return false;
}
var obj = {
        name: 'Usman',
        lName: 'Ghani',
        age: 23
   };
console.log(hasProperty(obj, 'name'));
console.log(hasProperty(obj, 'age'));
console.log(hasProperty(obj, 'nationality'));

