function Distance (...args) {
	return Math.sqrt(Math.pow(args[0]-args[2],2) + Math.pow(args[1]-args[3],2)).toFixed(2);
}
function Triangle (argument) {
	array = argument.map(x=>+x);
	let resultArr = [];
	let len = array.length;
	for(let i = 0; i < len; i += 4){
		resultArr.push(Distance(array[i], array[i+1], array[i+2], array[i+3]));
	}
	len = resultArr.length;
	let resultString = 'Triangle can\'t be formed';
	if ((resultArr[0]+resultArr[1] > resultArr[2]) && (resultArr[1]+resultArr[2] > resultArr[0]) && (resultArr[2]+resultArr[0] > resultArr[1])) {
	 		resultString = 'Triangle can be formed';
	 	}
	resultArr.push(resultString);
	return resultArr.join('\n');
}

console.log(Triangle([
  '5', '6', '7', '8',
  '1', '2', '3', '4',
  '9', '10', '11', '12'
]));