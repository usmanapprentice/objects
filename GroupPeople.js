
function groupBy(array) {
    let i, len = array.length;
        result = {};
        for(let value of array){
    	 if(!result[value.age]) {
              result[value.age] = [];
          }
         result[value.age].push(value);
    }
    return result;
}
function Action(){
	   var people = [
	  { firstname: 'Gosho', lastname: 'Petrov', age: 32 },
	  { firstname: 'Bay', lastname: 'Ivan', age: 81 },
	  { firstname: 'John', lastname: 'Doe', age: 42 },
	  { firstname: 'Pesho', lastname: 'Pesho', age: 22 },
	  { firstname: 'Asdf', lastname: 'Xyz', age: 81 },
	  { firstname: 'Gosho', lastname: 'Gosho', age: 22 }
	];
	groupedByAge= groupBy(people);
    console.log(groupedByAge);
}
Action();