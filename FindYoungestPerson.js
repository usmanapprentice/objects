function CreatePersonObject(name,lname,age) {
    var person = {
        name:name,
        lname:lname,
        age:age
    };
    return person;
}

function FindYoungest() {
    let arr = arguments[0];
    //console.log(arr);
    let arrObj =[];
    for (var i = 0; i < arr.length; i+=3) {
        let name = arr[i], lname = arr[i+1], age = arr[i+2];
        //console.log(name);
        arrObj.push(CreatePersonObject(name,lname,age));
    }
    let youngest = arrObj[0].age;
    let name = arrObj[0].name+' '+arrObj[0].lname+' : '+youngest;
    for(value of arrObj){
        if (value.age<youngest) {
            youngest = value.age;
            name = value.name+' '+value.lname+' : '+youngest;
        }
    }
    return name;
}

console.log(FindYoungest([
  'Gosho', 'Petrov', '32',
  'Bay', 'Ivan', '81',
  'John', 'Doe', '42'
]));