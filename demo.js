//Objects practice......
let student = {name:'Usman',
	age: 23, 
	nationality: 'Indian',
	grades: [6,6,6,5,6,6],
	getOlder: function () {this.age += 1;}
};
console.log(student);
student.getOlder();
console.log(student);

function makePerson (fname, lname) {
	return {
		fname: fname,
		lname: lname,
		toString: function () {
			return this.fname + ' ' + this.lname;
		},
	};
}

let Vera = makePerson('Vera', 'Dimitrova');
let Usman = makePerson('Usman', 'Ghani');

console.log(Vera.toString());
console.log(Usman.toString());
function Myfun(){
	for( i in document){
		largest = document[location];
		if (document[i] > largest) {
			largest = document[i];
		}
		return largest;
	 }
}

Myfun();

//removing redundancy.... from an array or sentence;
const wordArray = 'word pesho number word pesho word word monkey vsetaya'.split(' ');
let words = {};
for( let word of wordArray){
	if(words.hasOwnProperty(word)){
		words[word] += 1;
	} else{
		words[word] = 1;
	}
}

let arr = Object.keys(words);
console.log(arr);