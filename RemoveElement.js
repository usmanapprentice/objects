/*Removes every elements from array which are the same as the first element of array
 if no parameter given.*/
//If parameter provided, removes every element same as parameter from the array. 
Array.prototype.remove = function(k) {
	let key = k||this.shift();
	let foundIndex= -1;
	for(let value of this){
		if (value == key) {
			let index = this.indexOf(key, foundIndex+1)
			foundIndex =  index;
			this.splice(foundIndex, 1);
		}
	}
	return this.join('\n');	
};
console.log([ '_h/_',  '^54F#',  'V',  '^54F#',  'Z285',  'kv?tc`',  '^54F#',  '_h/_',  'Z285',
  			  '_h/_',  'kv?tc`',  'Z285',  '^54F#',  'Z285',  'Z285',  '_h/_',  '^54F#',  'kv?tc`',
			  'kv?tc`',  'Z285'].remove());
